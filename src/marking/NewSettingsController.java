package marking;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class NewSettingsController {

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private TextField surname;

	@FXML
	private TextField initials;

	@FXML
	private TextField rowIndex;

	@FXML
	private AnchorPane mainPane;

	@FXML
	private TextField checkStringDefault;

	@FXML
	private TextField zipSuffixDefault;

	@FXML
	private TextField mark;

	@FXML
	private TextField appendStringDefault;

	@FXML
	private TextField stNum;

	@FXML
	private CheckBox playBackground;

	@FXML
	private CheckBox playLoading;

	@FXML
	void saveClicked(ActionEvent event) {

		// Excel
		Main.settings.initialsColumnLetter = this.initials.getText();
		Main.settings.surnameColumnLetter = this.surname.getText();
		Main.settings.studentNumberColumnLetter = this.stNum.getText();
		Main.settings.markColumnLetter = this.mark.getText();
		Main.settings.startRow = Integer.parseInt(this.rowIndex.getText());

		// Java

		Main.settings.appendString = appendStringDefault.getText();
		Main.settings.CheckString = checkStringDefault.getText();
		Main.settings.zipSuffixString = zipSuffixDefault.getText();

		// Sound

		Main.settings.playBackgroundSound = playBackground.isSelected();
		Main.settings.playLoadingSound = playLoading.isSelected();

		FileIOManager.writeSettings();

	}


	@FXML
	void initialize() {
		assert playBackground != null : "fx:id=\"playBackground\" was not injected: check your FXML file 'NewSettingsGui.fxml'.";
		assert surname != null : "fx:id=\"surname\" was not injected: check your FXML file 'NewSettingsGui.fxml'.";
		assert initials != null : "fx:id=\"initials\" was not injected: check your FXML file 'NewSettingsGui.fxml'.";
		assert playLoading != null : "fx:id=\"playLoading\" was not injected: check your FXML file 'NewSettingsGui.fxml'.";
		assert rowIndex != null : "fx:id=\"rowIndex\" was not injected: check your FXML file 'NewSettingsGui.fxml'.";
		assert mainPane != null : "fx:id=\"mainPane\" was not injected: check your FXML file 'NewSettingsGui.fxml'.";
		assert checkStringDefault != null : "fx:id=\"checkStringDefault\" was not injected: check your FXML file 'NewSettingsGui.fxml'.";
		assert zipSuffixDefault != null : "fx:id=\"zipSuffixDefault\" was not injected: check your FXML file 'NewSettingsGui.fxml'.";
		assert mark != null : "fx:id=\"mark\" was not injected: check your FXML file 'NewSettingsGui.fxml'.";
		assert appendStringDefault != null : "fx:id=\"appendStringDefault\" was not injected: check your FXML file 'NewSettingsGui.fxml'.";
		assert stNum != null : "fx:id=\"stNum\" was not injected: check your FXML file 'NewSettingsGui.fxml'.";


		// Excel
		this.initials.setText(Main.settings.initialsColumnLetter);
		this.surname.setText(Main.settings.surnameColumnLetter);
		this.stNum.setText(Main.settings.studentNumberColumnLetter);
		this.mark.setText(Main.settings.markColumnLetter);
		this.rowIndex.setText(Main.settings.startRow+"");

		// Java

		this.appendStringDefault.setText(Main.settings.appendString);
		this.checkStringDefault.setText(Main.settings.CheckString);
		this.zipSuffixDefault.setText(Main.settings.zipSuffixString);

		// Sound
		
		this.playBackground.setSelected(Main.settings.playBackgroundSound);
		this.playLoading.setSelected(Main.settings.playLoadingSound);

	}
}
