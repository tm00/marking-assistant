package marking;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class FileIOManager {

	
	private static String fileDirectory=System.getProperty("user.dir");
	private static final String settingsFileName = "NewSettings.set";
	


	public static void writeSettings(){
		try{

			File f = new File(fileDirectory+"\\"+settingsFileName);
			System.out.println(f.getPath());
			FileOutputStream fout = new FileOutputStream(f);

			ObjectOutputStream oos = new ObjectOutputStream(fout); 

			//Create new database without historic data...
			NewSettings newdata = Main.settings;
						
			oos.writeObject(newdata);

			oos.close();
			System.out.println("Done");

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public static void readSettings(){
		NewSettings ingelees;
		try{
			File f = new File(fileDirectory+"/"+settingsFileName);
			FileInputStream fin = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(fin);
			ingelees = (NewSettings) ois.readObject();

			Main.settings = ingelees;

			ois.close();

		}catch(Exception ex){
			ex.printStackTrace();
			System.out.println("Settings not found, revertin to default");
			
		} 
	}
	
	public static void main(String[] args) {

		Main.settings = new NewSettings();
		Main.settings.setDefaults();
		
		writeSettings();
		
		
		readSettings();	System.out.println(Main.settings.toString());
		
		
		
	}
}
