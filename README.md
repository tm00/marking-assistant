# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

This program is created for:

1. Capturing the marks of written papers into an Excel file
2. Unzipping a zip file with electronic tests to be appended with the markers name and open them with notepad++
3. Read the text (student numbers) from a column in an excel file, append a piece of text at the end and print it to the screen

Admin stuff.

To run the program, make sure you have the Java Virtual Machine installed (version 7+), it is freely available on the interwebs.

NB: This program requires the following files/folders to be included within it's own directory.

1. loadingSound (folder)
2. Notepad++ (folder)
3. sounds (folder)
4. NewSettings.set (file)

Fun Features:  

1. This program plays a loading sound while you wait, you can change this sound by replacing the loading.mp3 in the 
	loadingSound folder. Do keep the name of the file.
2. This program can also play background music; you can set your own selection of .mp3 files in the sounds folder. 
3. Both of these functions can be disabled in the Settings tab in the program.

One last thing.

This program opens .java files with Notepad++, for this it must run on a windows system. It will not work with other systems. 
But some of the other functions will still work :) 

Happy marking.

For any bug reports, please contact the creator of this toolkit at theomuller00@gmail.com

* Version
0.9
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
Import as an eclipse existing project
* Configuration
Uses Java fx for user interface, so makse sure it is available
* Dependencies
All dependencies are included in the JARS folder


### Who do I talk to? ###

* Repo owner or admin
Theo - theomuller00@gmail.com